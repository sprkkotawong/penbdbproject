<!--HOME OWNER-->
<?php
function alert($msg) {
    echo "<script type='text/javascript'>alert('$msg');</script>";
}
?>

<?php
$error = false;
$error_message = "";


//create variable
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "DBpenb";

//create connection
$conn = new mysqli($servername,$username,$password,$dbname); 
?>

<!--REGISTER EMPLOYEE-->
<?php
session_start();
if(isset($_REQUEST['btn_register'])){
    if($_SESSION['arr']=='A'|| $_SESSION['arr']=='a'){
    if( empty($_POST["empID"]) || empty($_POST["fname"]) || empty($_POST["lname"]) 
    || empty($_POST["phone"]) || empty($_POST["gender"]) || empty($_POST["address"]) ){
        alert("Incomplete information");
    }else{
        $empID = $_POST["empID"];
        $fname = $_POST["fname"];
        $lname = $_POST["lname"];
        $phone = $_POST["phone"];
        $gender = $_POST["gender"];
        $address = $_POST["address"];
        $insRegister ="INSERT INTO employee VALUES('$empID', '$fname', '$lname', '$phone', '$gender', '$address') ";
        if (($conn->query($insRegister))){
            alert("Register Success!!");
        }
    }
}

};
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <link rel="stylesheet" type="text/css" href="styleHome.css" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Penb Database System</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
</head>

<body onload="startTime()" style="background-color: hsl(128, 0%, 96%);">
    <!--Nav-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom: 10px; top: 0;">
        <a class="navbar-brand" href="Home.php">Penb Internet Cafe</a>
        <span style="font-size: 30px; cursor: pointer;" onclick="openNav()">&#9776;</span>
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <a href="#" id="id-sidenav-reg-emp">- Register Employee</a>
            <!-- The Modal -->
            <div id="id-modal-re-emp" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close-reg-emp">&times;</span>
                    <!-- REGISTER EMP FORM (DONE) -->
                    <form action="Home.php" method="post">
                        <fieldset>
                            <legend>Register Employee:</legend>
                            <label for="empID-reg">Employee ID:</label>
                            <input type="text" id="empID" name="empID" /><br /><br />

                            <label for="fname-reg">First name:</label>
                            <input type="text" id="fname" name="fname" /><br /><br />

                            <label for="lname-reg">Last name:</label>
                            <input type="text" id="lname" name="lname" /><br /><br />

                            <label for="phone">Phone:</label>
                            <input type="number" id="phone" name="phone" /><br /><br />

                            <label for="gender">Gender:</label>
                            <select id="gender" name="gender">
                                <option value="M">M</option>
                                <option value="F">F</option>
                            </select><br /><br />
                            <label for="address">Address:</label>
                            <input type="text" id="address" name="address" /><br /><br />
                            <input type="submit" name="btn_register" value="submit" />
                        </fieldset>
                    </form>

                </div>
            </div>
            <a href="#" id="id-sidenav-search-emp">- Search Employee</a>
            <!-- The Modal -->
            <div id="id-modal-search-emp" class="modal">
                <!-- Modal content -->
                <div class="modal-content" id="search-odal-content">
                    <span class="close-search-emp">&times;</span>
                    <!-- SEARCH EMP FORM (DONE) -->
                    <form action="Result_Search_Employee.php" method="post">
                        <fieldset>
                            <legend>Search Employee:</legend>
                            <label for="empID-search">Employee ID:</label>
                            <input type="text" id="empID-search" name="empID-search" /><br />
                            <input type="submit" id="submit-emp-search" value="Submit" name="btn-search-emp" />
                        </fieldset>
                    </form>
                </div>
            </div>
            <a href="#" id="id-sidenav-search-cus">- Search Customer</a>
            <!-- The Modal -->
            <div id="id-modal-search-cus" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close-search-cus">&times;</span>
                    <!-- SEARCH CUS FORM (DONE) -->
                    <form action="Result_Search_Customer.php" method="post">
                        <fieldset>
                            <legend>Search Customer:</legend>
                            <label for="search-cus">Date:</label>
                            <input type="date" id="search-cus" name="search-cus" /><br /><br />
                            <input type="submit" name="btn-search-cus" value="Submit" />
                        </fieldset>
                    </form>
                </div>
            </div>

            <!-- <a href="#" id="id-sidenav-search-cafe">- Search Internet Cafe</a>
            
            <div id="id-modal-search-cafe" class="modal">
                
                <div class="modal-content">
                    <span class="close-search-cafe">&times;</span>
                    <form>
                        <fieldset>
                            <legend>Search Internet Cafe:</legend>
                            <label for="search-cus">Commercial ID:</label>
                            <input type="text" /><br /><br />
                            <input type="submit" value="Submit" />
                        </fieldset>
                    </form>
                </div>
            </div> -->
            <a href="#" id="id-sidenav-expense-cus-perday">- Expense Customer Per Day</a>
            <!-- The Modal -->
            <div id="id-modal-expense-cus-perday" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close-expense-cus-perday">&times;</span>
                    <!-- Expense Cus Per Day FORM (DONE) -->
                    <form action="Result_Expense_CusPD.php" method="post">
                        <fieldset>
                            <legend>Expense Customer Per Day:</legend>
                            <label for="exp-cus">Date:</label>
                            <input type="date" name="expense-cus-date" /><br /><br />
                            <input type="submit" name="expense-cus-pd-submit" value="Submit" />
                            <input type="submit" name="expense-cus-pd-view" value="View" />
                        </fieldset>
                    </form>
                </div>
            </div>
            <a href="#" id="id-sidenav-expense-emp-perday">- Expense Employee perday</a>
            <!-- The Modal -->
            <div id="id-modal-expense-emp-perday" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close-expense-emp-perday">&times;</span>
                    <!-- Expense Emp Per Day FORM (DONE) -->
                    <form action="Result_Expense_EmpPD.php" method="post">
                        <fieldset>
                            <legend>Expense Employee Per Day:</legend>
                            <label for="exp-emp">Date:</label>
                            <input type="date" name="expense-emp-date" /><br /><br />
                            <input type="submit" value="Submit" name="expense-emp-pd-submit" />
                            <input type="submit" value="View" name="expense-emp-pd-view" />
                        </fieldset>
                    </form>
                </div>
            </div>
            <a href="#" id="id-sidenav-expense-daily">- Daily Total Expense</a>
            <!-- The Modal -->
            <div id="id-modal-expense-daily" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close-expense-daily">&times;</span>
                    <!-- Expense Per Day FORM (DONE) -->
                    <form action="Result_Expense_PD.php" method="post">
                        <fieldset>
                            <legend>Expense Daily:</legend>
                            <label for="exp-pd">Date:</label>
                            <input type="date" name="expense-pd-date" /><br /><br />
                            <input type="submit" value="Submit" name="expense-pd-submit" />
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- <a href="#" id="id-sidenav-delete-emp">- Deletee Employee</a>
            <div id="id-modal-delete-emp" class="modal">
                <div class="modal-content">
                    <span class="close-delete-emp">&times;</span>
                    <form>
                        <fieldset>
                            <legend>Delete Employee:</legend>
                            <label for="del-emp">Employee ID:</label>
                            <input type="text" /><br /><br />
                            <input type="submit" value="Submit" />
                        </fieldset>
                    </form>
                </div>
            </div>  -->
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto"></ul>
            <form action="Logout.php" class="form-inline my-2 my-lg-0">
                <button class="btn btn-outline-success my-2 my-sm-0" id="btn-logout" type="submit">
                    Logout
                </button>
            </form>
        </div>
    </nav>
    <!--Nav-->
    <!--container-->
    <div class="container">
        <!--row-->
        <div class="row">
            <div class="col-md-2"></div>

            <!--col-md-10-->
            <div class="col-md-10">
                <center>
                    <div id="showDate">
                        <p id="showTime"></p>
                    </div>
                </center>
                <!--Search-->
                <input type="text" id="myInput" onkeyup="searchComputer()" placeholder="Search for Card number" />
                <table id="myTable">
                    <tr class="header">
                        <th style="width: 45%;">Card No.</th>
                        <th style="width: 55%;">Status</th>
                    </tr>
                    <tr id="tr1">
                        <td>
                            1<br />
                            <p id="startActive1" name="startActive1">START : </p>
                            <p id="stopPay1" name="stopPay1">STOP : </p>
                            <p id="total1" name="total1">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_1" name="form_act_pay_del_clr_1" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel1" name="Tel1"></input><br><br>
                                <button class="btn-active" id="btn-active-1" name="btn-active-1" type="button"
                                    onclick="activeBtn('Tel1','startActive1','btn-active-1','btn-pay-1','btn-delete-1','btn-clear-1','tr1',1)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-1" name="btn-pay-1" type="button" disabled
                                    onclick="payBtn('stopPay1', 'btn-active-1', 'btn-pay-1', 'btn-delete-1', 'btn-clear-1','total1',1)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-1" type="button" disabled
                                    onclick="deleteBtn('startActive1','btn-active-1','btn-pay-1', 'btn-delete-1', 'btn-clear-1', 1); document.getElementById('tr1').style.backgroundColor=''; document.getElementById('Tel1').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-1" type="button" disabled
                                    onclick="clear_crd('startActive1', 'stopPay1','btn-active-1','btn-pay-1','total1'); document.getElementById('tr1').style.backgroundColor=''; document.getElementById('Tel1').value = ''; ">
                                    Clear
                                </button>

                            </form>
                        </td>
                    </tr>
                    <tr id="tr2">
                        <td>
                            2<br />
                            <p id="startActive2" name="startActive2">START : </p>
                            <p id="stopPay2" name="stopPay2">STOP : </p>
                            <p id="total2" name="total2">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_2" name="form_act_pay_del_clr_2" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel2" name="Tel2"></input><br><br>
                                <button class="btn-active" id="btn-active-2" name="btn-active-2" type="button"
                                    onclick="activeBtn('Tel2','startActive2','btn-active-2','btn-pay-2','btn-delete-2','btn-clear-2','tr2',2)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-2" name="btn-pay-2" type="button" disabled
                                    onclick="payBtn('stopPay2', 'btn-active-2', 'btn-pay-2', 'btn-delete-2', 'btn-clear-2','total2',2)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-2" type="button" disabled
                                    onclick="deleteBtn('startActive2','btn-active-2','btn-pay-2', 'btn-delete-2', 'btn-clear-2', 2); document.getElementById('tr2').style.backgroundColor=''; document.getElementById('Tel2').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-2" type="button" disabled
                                    onclick="clear_crd('startActive2', 'stopPay2','btn-active-2','btn-pay-2','total2'); document.getElementById('tr2').style.backgroundColor=''; document.getElementById('Tel2').value = ''; ">
                                    Clear
                                </button>

                            </form>
                        </td>
                    </tr>
                    <tr id="tr3">
                        <td>
                            3<br />
                            <p id="startActive3" name="startActive3">START : </p>
                            <p id="stopPay3" name="stopPay3">STOP : </p>
                            <p id="total3" name="total3">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_3" name="form_act_pay_del_clr_3" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel3" name="Tel3"></input><br><br>
                                <button class="btn-active" id="btn-active-3" name="btn-active-3" type="button"
                                    onclick="activeBtn('Tel3','startActive3','btn-active-3','btn-pay-3','btn-delete-3','btn-clear-3','tr3',3)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-3" name="btn-pay-3" type="button" disabled
                                    onclick="payBtn('stopPay3', 'btn-active-3', 'btn-pay-3', 'btn-delete-3', 'btn-clear-3','total3',3)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-3" type="button" disabled
                                    onclick="deleteBtn('startActive3','btn-active-3','btn-pay-3', 'btn-delete-3', 'btn-clear-3', 3); document.getElementById('tr3').style.backgroundColor=''; document.getElementById('Tel3').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-3" type="button" disabled
                                    onclick="clear_crd('startActive3', 'stopPay3','btn-active-3','btn-pay-3','total3'); document.getElementById('tr3').style.backgroundColor=''; document.getElementById('Tel3').value = ''; ">
                                    Clear
                                </button>

                            </form>
                        </td>
                    </tr>
                    <tr id="tr4">
                        <td>
                            4<br />
                            <p id="startActive4" name="startActive4">START : </p>
                            <p id="stopPay4" name="stopPay4">STOP : </p>
                            <p id="total4" name="total4">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_4" name="form_act_pay_del_clr_4" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel4" name="Tel4"></input><br><br>
                                <button class="btn-active" id="btn-active-4" name="btn-active-4" type="button"
                                    onclick="activeBtn('Tel4','startActive4','btn-active-4','btn-pay-4','btn-delete-4','btn-clear-4','tr4',4)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-4" name="btn-pay-4" type="button" disabled
                                    onclick="payBtn('stopPay4', 'btn-active-4', 'btn-pay-4', 'btn-delete-4', 'btn-clear-4','total4',4)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-4" type="button" disabled
                                    onclick="deleteBtn('startActive4','btn-active-4','btn-pay-4', 'btn-delete-4', 'btn-clear-4', 4); document.getElementById('tr4').style.backgroundColor=''; document.getElementById('Tel4').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-4" type="button" disabled
                                    onclick="clear_crd('startActive4', 'stopPay4','btn-active-4','btn-pay-4','total4'); document.getElementById('tr4').style.backgroundColor=''; document.getElementById('Tel4').value = ''; ">
                                    Clear
                                </button>

                            </form>
                        </td>
                    </tr>
                    <tr id="tr5">
                        <td>
                            5<br />
                            <p id="startActive5" name="startActive5">START : </p>
                            <p id="stopPay5" name="stopPay5">STOP : </p>
                            <p id="total5" name="total5">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_5" name="form_act_pay_del_clr_5" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel5" name="Tel5"></input><br><br>
                                <button class="btn-active" id="btn-active-5" name="btn-active-5" type="button"
                                    onclick="activeBtn('Tel5','startActive5','btn-active-5','btn-pay-5','btn-delete-5','btn-clear-5','tr5',5)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-5" name="btn-pay-5" type="button" disabled
                                    onclick="payBtn('stopPay5', 'btn-active-5', 'btn-pay-5', 'btn-delete-5', 'btn-clear-5','total5',5)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-5" type="button" disabled
                                    onclick="deleteBtn('startActive5','btn-active-5','btn-pay-5', 'btn-delete-5', 'btn-clear-5', 5); document.getElementById('tr5').style.backgroundColor=''; document.getElementById('Tel5').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-5" type="button" disabled
                                    onclick="clear_crd('startActive5', 'stopPay5','btn-active-5','btn-pay-5','total5'); document.getElementById('tr5').style.backgroundColor=''; document.getElementById('Tel5').value = ''; ">
                                    Clear
                                </button>

                            </form>
                        </td>
                    </tr>
                    <tr id="tr6">
                        <td>
                            6<br />
                            <p id="startActive6" name="startActive6">START : </p>
                            <p id="stopPay6" name="stopPay6">STOP : </p>
                            <p id="total6" name="total6">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_6" name="form_act_pay_del_clr_6" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel6" name="Tel6"></input><br><br>
                                <button class="btn-active" id="btn-active-6" name="btn-active-6" type="button"
                                    onclick="activeBtn('Tel6','startActive6','btn-active-6','btn-pay-6','btn-delete-6','btn-clear-6','tr6',6)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-6" name="btn-pay-6" type="button" disabled
                                    onclick="payBtn('stopPay6', 'btn-active-6', 'btn-pay-6', 'btn-delete-6', 'btn-clear-6','total6',6)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-6" type="button" disabled
                                    onclick="deleteBtn('startActive6','btn-active-6','btn-pay-6', 'btn-delete-6', 'btn-clear-6', 6); document.getElementById('tr6').style.backgroundColor=''; document.getElementById('Tel6').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-6" type="button" disabled
                                    onclick="clear_crd('startActive6', 'stopPay6','btn-active-6','btn-pay-6','total6'); document.getElementById('tr6').style.backgroundColor=''; document.getElementById('Tel6').value = ''; ">
                                    Clear
                                </button>

                            </form>
                        </td>
                    </tr>
                    <tr id="tr7">
                        <td>
                            7<br />
                            <p id="startActive7" name="startActive7">START : </p>
                            <p id="stopPay7" name="stopPay7">STOP : </p>
                            <p id="total7" name="total7">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_7" name="form_act_pay_del_clr_7" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel7" name="Tel7"></input><br><br>
                                <button class="btn-active" id="btn-active-7" name="btn-active-7" type="button"
                                    onclick="activeBtn('Tel7','startActive7','btn-active-7','btn-pay-7','btn-delete-7','btn-clear-7','tr7',7)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-7" name="btn-pay-7" type="button" disabled
                                    onclick="payBtn('stopPay7', 'btn-active-7', 'btn-pay-7', 'btn-delete-7', 'btn-clear-7','total7',7)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-7" type="button" disabled
                                    onclick="deleteBtn('startActive7','btn-active-7','btn-pay-7', 'btn-delete-7', 'btn-clear-7', 7); document.getElementById('tr7').style.backgroundColor=''; document.getElementById('Tel7').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-7" type="button" disabled
                                    onclick="clear_crd('startActive7', 'stopPay7','btn-active-7','btn-pay-7','total7'); document.getElementById('tr7').style.backgroundColor=''; document.getElementById('Tel7').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr8">
                        <td>
                            8<br />
                            <p id="startActive8" name="startActive8">START : </p>
                            <p id="stopPay8" name="stopPay8">STOP : </p>
                            <p id="total8" name="total8">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_8" name="form_act_pay_del_clr_8" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel8" name="Tel8"></input><br><br>
                                <button class="btn-active" id="btn-active-8" name="btn-active-8" type="button"
                                    onclick="activeBtn('Tel8','startActive8','btn-active-8','btn-pay-8','btn-delete-8','btn-clear-8','tr8',8)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-8" name="btn-pay-8" type="button" disabled
                                    onclick="payBtn('stopPay8', 'btn-active-8', 'btn-pay-8', 'btn-delete-8', 'btn-clear-8','total8',8)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-8" type="button" disabled
                                    onclick="deleteBtn('startActive8','btn-active-8','btn-pay-8', 'btn-delete-8', 'btn-clear-8', 8); document.getElementById('tr8').style.backgroundColor=''; document.getElementById('Tel8').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-8" type="button" disabled
                                    onclick="clear_crd('startActive8', 'stopPay8','btn-active-8','btn-pay-8','total8'); document.getElementById('tr8').style.backgroundColor=''; document.getElementById('Tel8').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr9">
                        <td>
                            9<br />
                            <p id="startActive9" name="startActive9">START : </p>
                            <p id="stopPay9" name="stopPay9">STOP : </p>
                            <p id="total9" name="total9">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_9" name="form_act_pay_del_clr_9" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel9" name="Tel9"></input><br><br>
                                <button class="btn-active" id="btn-active-9" name="btn-active-9" type="button"
                                    onclick="activeBtn('Tel9','startActive9','btn-active-9','btn-pay-9','btn-delete-9','btn-clear-9','tr9',9)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-9" name="btn-pay-9" type="button" disabled
                                    onclick="payBtn('stopPay9', 'btn-active-9', 'btn-pay-9', 'btn-delete-9', 'btn-clear-9','total9',9)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-9" type="button" disabled
                                    onclick="deleteBtn('startActive9','btn-active-9','btn-pay-9', 'btn-delete-9', 'btn-clear-9', 9); document.getElementById('tr9').style.backgroundColor=''; document.getElementById('Tel9').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-9" type="button" disabled
                                    onclick="clear_crd('startActive9', 'stopPay9','btn-active-9','btn-pay-9','total9'); document.getElementById('tr9').style.backgroundColor=''; document.getElementById('Tel9').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr10">
                        <td>
                            10<br />
                            <p id="startActive10" name="startActive10">START : </p>
                            <p id="stopPay10" name="stopPay10">STOP : </p>
                            <p id="total10" name="total10">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_10" name="form_act_pay_del_clr_10" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel10" name="Tel10"></input><br><br>
                                <button class="btn-active" id="btn-active-10" name="btn-active-10" type="button"
                                    onclick="activeBtn('Tel10','startActive10','btn-active-10','btn-pay-10','btn-delete-10','btn-clear-10','tr10',10)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-10" name="btn-pay-10" type="button" disabled
                                    onclick="payBtn('stopPay10', 'btn-active-10', 'btn-pay-10', 'btn-delete-10', 'btn-clear-10','total10',10)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-10" type="button" disabled
                                    onclick="deleteBtn('startActive10','btn-active-10','btn-pay-10', 'btn-delete-10', 'btn-clear-10', 10); document.getElementById('tr10').style.backgroundColor=''; document.getElementById('Tel10').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-10" type="button" disabled
                                    onclick="clear_crd('startActive10', 'stopPay10','btn-active-10','btn-pay-10','total10'); document.getElementById('tr10').style.backgroundColor=''; document.getElementById('Tel10').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr11">
                        <td>
                            11<br />
                            <p id="startActive11" name="startActive11">START : </p>
                            <p id="stopPay11" name="stopPay11">STOP : </p>
                            <p id="total11" name="total11">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_11" name="form_act_pay_del_clr_11" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel11" name="Tel11"></input><br><br>
                                <button class="btn-active" id="btn-active-11" name="btn-active-11" type="button"
                                    onclick="activeBtn('Tel11','startActive11','btn-active-11','btn-pay-11','btn-delete-11','btn-clear-11','tr11',11)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-11" name="btn-pay-11" type="button" disabled
                                    onclick="payBtn('stopPay11', 'btn-active-11', 'btn-pay-11', 'btn-delete-11', 'btn-clear-11','total11',11)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-11" type="button" disabled
                                    onclick="deleteBtn('startActive11','btn-active-11','btn-pay-11', 'btn-delete-11', 'btn-clear-11', 11); document.getElementById('tr11').style.backgroundColor=''; document.getElementById('Tel11').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-11" type="button" disabled
                                    onclick="clear_crd('startActive11', 'stopPay11','btn-active-11','btn-pay-11','total11'); document.getElementById('tr11').style.backgroundColor=''; document.getElementById('Tel11').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr12">
                        <td>
                            12<br />
                            <p id="startActive12" name="startActive12">START : </p>
                            <p id="stopPay12" name="stopPay12">STOP : </p>
                            <p id="total12" name="total12">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_12" name="form_act_pay_del_clr_12" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel12" name="Tel12"></input><br><br>
                                <button class="btn-active" id="btn-active-12" name="btn-active-12" type="button"
                                    onclick="activeBtn('Tel12','startActive12','btn-active-12','btn-pay-12','btn-delete-12','btn-clear-12','tr12',12)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-12" name="btn-pay-12" type="button" disabled
                                    onclick="payBtn('stopPay12', 'btn-active-12', 'btn-pay-12', 'btn-delete-12', 'btn-clear-12','total12',12)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-12" type="button" disabled
                                    onclick="deleteBtn('startActive12','btn-active-12','btn-pay-12', 'btn-delete-12', 'btn-clear-12', 12); document.getElementById('tr12').style.backgroundColor=''; document.getElementById('Tel12').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-12" type="button" disabled
                                    onclick="clear_crd('startActive12', 'stopPay12','btn-active-12','btn-pay-12','total12'); document.getElementById('tr12').style.backgroundColor=''; document.getElementById('Tel12').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr13">
                        <td>
                            13<br />
                            <p id="startActive13" name="startActive13">START : </p>
                            <p id="stopPay13" name="stopPay13">STOP : </p>
                            <p id="total13" name="total13">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_13" name="form_act_pay_del_clr_13" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel13" name="Tel13"></input><br><br>
                                <button class="btn-active" id="btn-active-13" name="btn-active-13" type="button"
                                    onclick="activeBtn('Tel13','startActive13','btn-active-13','btn-pay-13','btn-delete-13','btn-clear-13','tr13',13)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-13" name="btn-pay-13" type="button" disabled
                                    onclick="payBtn('stopPay13', 'btn-active-13', 'btn-pay-13', 'btn-delete-13', 'btn-clear-13','total13',13)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-13" type="button" disabled
                                    onclick="deleteBtn('startActive13','btn-active-13','btn-pay-13', 'btn-delete-13', 'btn-clear-13', 13); document.getElementById('tr13').style.backgroundColor=''; document.getElementById('Tel13').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-13" type="button" disabled
                                    onclick="clear_crd('startActive13', 'stopPay13','btn-active-13','btn-pay-13','total13'); document.getElementById('tr13').style.backgroundColor=''; document.getElementById('Tel13').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr14">
                        <td>
                            14<br />
                            <p id="startActive14" name="startActive14">START : </p>
                            <p id="stopPay14" name="stopPay14">STOP : </p>
                            <p id="total14" name="total14">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_14" name="form_act_pay_del_clr_14" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel14" name="Tel14"></input><br><br>
                                <button class="btn-active" id="btn-active-14" name="btn-active-14" type="button"
                                    onclick="activeBtn('Tel14','startActive14','btn-active-14','btn-pay-14','btn-delete-14','btn-clear-14','tr14',14)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-14" name="btn-pay-14" type="button" disabled
                                    onclick="payBtn('stopPay14', 'btn-active-14', 'btn-pay-14', 'btn-delete-14', 'btn-clear-14','total14',14)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-14" type="button" disabled
                                    onclick="deleteBtn('startActive14','btn-active-14','btn-pay-14', 'btn-delete-14', 'btn-clear-14', 14); document.getElementById('tr14').style.backgroundColor=''; document.getElementById('Tel14').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-14" type="button" disabled
                                    onclick="clear_crd('startActive14', 'stopPay14','btn-active-14','btn-pay-14','total14'); document.getElementById('tr14').style.backgroundColor=''; document.getElementById('Tel14').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr15">
                        <td>
                            15<br />
                            <p id="startActive15" name="startActive15">START : </p>
                            <p id="stopPay15" name="stopPay15">STOP : </p>
                            <p id="total15" name="total15">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_15" name="form_act_pay_del_clr_15" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel15" name="Tel15"></input><br><br>
                                <button class="btn-active" id="btn-active-15" name="btn-active-15" type="button"
                                    onclick="activeBtn('Tel15','startActive15','btn-active-15','btn-pay-15','btn-delete-15','btn-clear-15','tr15',15)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-15" name="btn-pay-15" type="button" disabled
                                    onclick="payBtn('stopPay15', 'btn-active-15', 'btn-pay-15', 'btn-delete-15', 'btn-clear-15','total15',15)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-15" type="button" disabled
                                    onclick="deleteBtn('startActive15','btn-active-15','btn-pay-15', 'btn-delete-15', 'btn-clear-15', 15); document.getElementById('tr15').style.backgroundColor=''; document.getElementById('Tel15').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-15" type="button" disabled
                                    onclick="clear_crd('startActive15', 'stopPay15','btn-active-15','btn-pay-15','total15'); document.getElementById('tr15').style.backgroundColor=''; document.getElementById('Tel15').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr16">
                        <td>
                            16<br />
                            <p id="startActive16" name="startActive16">START : </p>
                            <p id="stopPay16" name="stopPay16">STOP : </p>
                            <p id="total16" name="total16">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_16" name="form_act_pay_del_clr_16" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel16" name="Tel16"></input><br><br>
                                <button class="btn-active" id="btn-active-16" name="btn-active-16" type="button"
                                    onclick="activeBtn('Tel16','startActive16','btn-active-16','btn-pay-16','btn-delete-16','btn-clear-16','tr16',16)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-16" name="btn-pay-16" type="button" disabled
                                    onclick="payBtn('stopPay16', 'btn-active-16', 'btn-pay-16', 'btn-delete-16', 'btn-clear-16','total16',16)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-16" type="button" disabled
                                    onclick="deleteBtn('startActive16','btn-active-16','btn-pay-16', 'btn-delete-16', 'btn-clear-16', 16); document.getElementById('tr16').style.backgroundColor=''; document.getElementById('Tel16').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-16" type="button" disabled
                                    onclick="clear_crd('startActive16', 'stopPay16','btn-active-16','btn-pay-16','total16'); document.getElementById('tr16').style.backgroundColor=''; document.getElementById('Tel16').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr17">
                        <td>
                            17<br />
                            <p id="startActive17" name="startActive17">START : </p>
                            <p id="stopPay17" name="stopPay17">STOP : </p>
                            <p id="total17" name="total17">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_17" name="form_act_pay_del_clr_17" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel17" name="Tel17"></input><br><br>
                                <button class="btn-active" id="btn-active-17" name="btn-active-17" type="button"
                                    onclick="activeBtn('Tel17','startActive17','btn-active-17','btn-pay-17','btn-delete-17','btn-clear-17','tr17',17)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-17" name="btn-pay-17" type="button" disabled
                                    onclick="payBtn('stopPay17', 'btn-active-17', 'btn-pay-17', 'btn-delete-17', 'btn-clear-17','total17',17)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-17" type="button" disabled
                                    onclick="deleteBtn('startActive17','btn-active-17','btn-pay-17', 'btn-delete-17', 'btn-clear-17', 17); document.getElementById('tr17').style.backgroundColor=''; document.getElementById('Tel17').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-17" type="button" disabled
                                    onclick="clear_crd('startActive17', 'stopPay17','btn-active-17','btn-pay-17','total17'); document.getElementById('tr17').style.backgroundColor=''; document.getElementById('Tel17').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr18">
                        <td>
                            18<br />
                            <p id="startActive18" name="startActive18">START : </p>
                            <p id="stopPay18" name="stopPay18">STOP : </p>
                            <p id="total18" name="total18">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_18" name="form_act_pay_del_clr_18" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel18" name="Tel18"></input><br><br>
                                <button class="btn-active" id="btn-active-18" name="btn-active-18" type="button"
                                    onclick="activeBtn('Tel18','startActive18','btn-active-18','btn-pay-18','btn-delete-18','btn-clear-18','tr18',18)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-18" name="btn-pay-18" type="button" disabled
                                    onclick="payBtn('stopPay18', 'btn-active-18', 'btn-pay-18', 'btn-delete-18', 'btn-clear-18','total18',18)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-18" type="button" disabled
                                    onclick="deleteBtn('startActive18','btn-active-18','btn-pay-18', 'btn-delete-18', 'btn-clear-18', 18); document.getElementById('tr18').style.backgroundColor=''; document.getElementById('Tel18').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-18" type="button" disabled
                                    onclick="clear_crd('startActive18', 'stopPay18','btn-active-18','btn-pay-18','total18'); document.getElementById('tr18').style.backgroundColor=''; document.getElementById('Tel18').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr19">
                        <td>
                            19<br />
                            <p id="startActive19" name="startActive19">START : </p>
                            <p id="stopPay19" name="stopPay19">STOP : </p>
                            <p id="total19" name="total19">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_19" name="form_act_pay_del_clr_19" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel19" name="Tel19"></input><br><br>
                                <button class="btn-active" id="btn-active-19" name="btn-active-19" type="button"
                                    onclick="activeBtn('Tel19','startActive19','btn-active-19','btn-pay-19','btn-delete-19','btn-clear-19','tr19',19)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-19" name="btn-pay-19" type="button" disabled
                                    onclick="payBtn('stopPay19', 'btn-active-19', 'btn-pay-19', 'btn-delete-19', 'btn-clear-19','total19',19)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-19" type="button" disabled
                                    onclick="deleteBtn('startActive19','btn-active-19','btn-pay-19', 'btn-delete-19', 'btn-clear-19', 19); document.getElementById('tr19').style.backgroundColor=''; document.getElementById('Tel19').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-19" type="button" disabled
                                    onclick="clear_crd('startActive19', 'stopPay19','btn-active-19','btn-pay-19','total19'); document.getElementById('tr19').style.backgroundColor=''; document.getElementById('Tel19').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>
                    <tr id="tr20">
                        <td>
                            20<br />
                            <p id="startActive20" name="startActive20">START : </p>
                            <p id="stopPay20" name="stopPay20">STOP : </p>
                            <p id="total20" name="total20">TOTAL : </p>
                        </td>
                        <td>
                            <form action="" id="form_act_pay_del_clr_20" name="form_act_pay_del_clr_20" method="post">
                                <label>TEL :</label>
                                <input class="InputTel" type="text" id="Tel20" name="Tel20"></input><br><br>
                                <button class="btn-active" id="btn-active-20" name="btn-active-20" type="button"
                                    onclick="activeBtn('Tel20','startActive20','btn-active-20','btn-pay-20','btn-delete-20','btn-clear-20','tr20',20)">
                                    Active
                                </button>
                                <button class="btn-pay" id="btn-pay-20" name="btn-pay-20" type="button" disabled
                                    onclick="payBtn('stopPay20', 'btn-active-20', 'btn-pay-20', 'btn-delete-20', 'btn-clear-20','total20',20)">
                                    Pay
                                </button>
                                <button class="btn-delete" id="btn-delete-20" type="button" disabled
                                    onclick="deleteBtn('startActive20','btn-active-20','btn-pay-20', 'btn-delete-20', 'btn-clear-20', 20); document.getElementById('tr20').style.backgroundColor=''; document.getElementById('Tel20').value = ''; ">
                                    Delete
                                </button>
                                <button class="btn-clear" id="btn-clear-20" type="button" disabled
                                    onclick="clear_crd('startActive20', 'stopPay20','btn-active-20','btn-pay-20','total20'); document.getElementById('tr20').style.backgroundColor=''; document.getElementById('Tel20').value = ''; ">
                                    Clear
                                </button>
                            </form>
                        </td>
                    </tr>

                </table>
                <!--Search-->
            </div>
            <!--col-md-8-->
        </div>
        <!--row-->
    </div>
    <script type="text/javascript">
    //active btn
    function activeBtn(tel, actStatus, btnID, payID, delID, clrID, tr, crdNo) {
        $(document).ready(function() {
            activeCrd(tel, actStatus, btnID, payID, delID, clrID);
            document.getElementById(tr).style.backgroundColor = 'hsl(0, 100%, 70%)';
            const Tel = document.getElementById(tel).value;
            var jsonActObj = {
                "tel": Tel,
                "crdNo": crdNo
            };
            $.ajax({
                type: "POST",
                url: "ActiveBtn.php",
                data: jsonActObj,
                success: function(result) {},
            });
        });
    }
    //pay btn
    function payBtn(stopStatus, actbtn, paybtn, delbtn, clrbtn, totalID, crdNo) {
        $(document).ready(function() {
            payCrd(stopStatus, actbtn, paybtn, delbtn, clrbtn);
            var jsonPayObj = {
                "crdNo": crdNo
            }
            $.ajax({
                type: "POST",
                url: "PayBtn.php",
                data: jsonPayObj,
                success: function(data) {
                    var totalPrice = "Total : ";
                    for (var key in data) {
                        totalPrice += data[key];
                    }
                    document.getElementById(totalID).innerHTML = totalPrice;
                },
            });
        });
    }

    //delete btn
    function deleteBtn(payID, actbtn, paybtn, delbtn, clrbtn, crdNo) {
        $(document).ready(function() {
            deleteCrd(payID, actbtn, paybtn, delbtn, clrbtn);
            var jsonDelObj = {
                "crdNo": crdNo
            }
            $.ajax({
                type: "POST",
                url: "DeleteBtn.php",
                data: jsonDelObj,
                success: function(result) {
                    alert(result)
                },
            });
        });
    }
    </script>
</body>
<script type="text/javascript" src="JsHome.js"></script>

</html>